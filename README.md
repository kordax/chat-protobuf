# Chat Protocol Buffers library readme

##Prerequisites:
1) Protocol Buffers compiler; (Available at "https://github.com/google/protobuf/releases", binary packages are named protoc-3.6.0-*);
2) Java > 11; (e.g. OpenJDK 11);
3) Gradle > 4.12.*;
4) Scala >2.12.7;
5) SBT (Scala Build Tool).

##To build:
###Java classes + library
run `gradle build` or `gradle build -x test` to avoid tests.
###Scala classes + library
run `sbt package` command.

