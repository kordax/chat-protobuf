/*
 *
 * [build_scala.gradle]
 * [chat-protobuf]
 *
 * Created by [Dmitry Morozov] on 05 September 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 *
 */
name := "chat-protobuf"
version := "0.5.64.2"
scalaVersion := "2.12.7"

// Ignore src directory with Java files
unmanagedSourceDirectories in Compile := Seq()

PB.protoSources in Compile := Seq(file("src/test/resources/protobuf"))

PB.targets in Compile := Seq(
        scalapb.gen() -> (sourceManaged in Compile).value
)
