/*
 * [TaskDeserializer.java]
 * [chat-protobuf]
 *
 * Created by [Dmitry Morozov] on 14 November 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatprotobuf.kafka;

import com.google.protobuf.InvalidProtocolBufferException;
import net.arammeem.chatprotobuf.messages.Backend.Task;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;

import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * ProtoBuf messages deserializer for Kafka
 *
 */
public class TaskDeserializer implements Deserializer<Task> {
    private Logger LOGGER = getLogger(TaskDeserializer.class);
    /**
     * Configure this class.
     *
     * @param configs configs in key/value pairs
     * @param isKey whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        LOGGER.debug("Configuring deserializer");
    }

    /**
     * Deserialize a record value from a byte array into a value or object.
     *
     * @param topic topic associated with the data
     * @param data serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
     * @return deserialized typed data; may be null
     */
    @Override
    public Task deserialize(String topic, byte[] data) {
        try {
            return Task.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.error("Failed to deserialize PBMessage for topic {}", topic);
            return null;
        }
    }

    @Override
    public void close() {
    // There's nothing to do on close for this deserializer
    }
}