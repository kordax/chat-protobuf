/*
 * [TaskSerializer.java]
 * [chat-protobuf]
 *
 * Created by [Dmitry Morozov] on 14 November 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatprotobuf.kafka;

import net.arammeem.chatprotobuf.messages.Backend.Task;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;

import java.util.Map;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * ProtoBuf messages deserializer for Kafka
 *
 */
public class TaskSerializer implements Serializer<Task> {
    private Logger LOGGER = getLogger(TaskSerializer.class);
    /**
     * Configure this class.
     *
     * @param configs configs in key/value pairs
     * @param isKey whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        LOGGER.debug("Configuring deserializer");
    }

    /**
     * Convert `data` into a byte array.
     *
     * @param topic topic associated with data
     * @param data typed data
     * @return serialized bytes
     */
    @Override
    public byte[] serialize(String topic, Task data) {
        if (data == null) {
            LOGGER.error("Failed to serialize PBMessage for topic {}, data is null!", topic);

            return new byte[0];
        }

        return data.toByteArray();
    }

    /**
     * Close this serializer.
     *
     * This method must be idempotent as it may be called multiple times.
     */
    @Override
    public void close() {
    // There's nothing to do on close for this serializer
    }
}