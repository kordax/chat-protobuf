/*
 * [DeserializationTests.java]
 * [chat-protobuf]
 *
 * Created by [Dmitry Morozov] on 14 November 2018.
 * Copyright (c) 2018 Aram Meem Company Limited. All rights reserved.
 */

package net.arammeem.chatbackend.tests;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import net.arammeem.chatprotobuf.messages.Client;
import net.arammeem.chatprotobuf.messages.internal.Internal;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * Processing tests.
 * These are main business logic tests.
 *
 */
@RunWith(JUnitPlatform.class)
public class DeserializationTests {
    /**
     * Process PEER_TO_PEER CreateConversation task.
     *
     * @case positive: PEER_TO_PEER conversation type
     * @result we should receive successful response with valid parameters
     */
    @Test
    void Should_DeserializeCreateConversationRequest_When_AllIsFine() throws IOException {
        Client.Request.CreateConversation request = Client.Request.CreateConversation.newBuilder()
                        .setType(Internal.Conversation.Type.PEER_TO_PEER)
                        .setInitialMessage(Internal.Message.newBuilder()
                                .addParts(Internal.MessagePart.newBuilder()
                                        .setBody(ByteString.copyFromUtf8("title"))
                                        .setContentType("merchant/toyou.1")
                                )
                        )
                        .setTitle("title")
                        .addUserIds(UUID.randomUUID().toString()
                        ).build();

        writeMessageToFile("Should_DeserializeCreateConversationRequest_When_AllIsFine.txt", request);

        byte[] bytes = readBytesFromFile("Should_DeserializeCreateConversationRequest_When_AllIsFine.txt");
        Files.delete(Paths.get("Should_DeserializeCreateConversationRequest_When_AllIsFine.txt"));
        Client.Request.CreateConversation.parseFrom(bytes);
    }

    /**
     * Write protobuf ver. 3 message to file
     *
     * @param fileName file name
     * @param messageV3 message of {@link GeneratedMessageV3} type
     */
    private void writeMessageToFile(String fileName, GeneratedMessageV3 messageV3) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(new File(fileName))) {
            fos.write(messageV3.toByteArray());
            fos.flush();
        }
    }

    /**
     * Read protobuf ver. 3 message bytes from file
     *
     * @param fileName file name
     */
    private byte[] readBytesFromFile(String fileName) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(fileName));

        return bytes;
    }
}